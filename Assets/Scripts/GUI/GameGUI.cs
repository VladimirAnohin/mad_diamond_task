﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGUI : MonoBehaviour
{
    [SerializeField]
    private FirstNPC first;

    private void OnGUI()
    {
        if (GUI.Button(new Rect(10, 10, 100, 50), "Change state"))
        {
            first.ChangeState();
        }
    }

}
