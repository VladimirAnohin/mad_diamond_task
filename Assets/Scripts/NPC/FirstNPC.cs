﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstNPC : NPC
{
    [SerializeField]
    private IdleDestinationPoint idlePoint;

    [SerializeField]
    private float timeBetweenAtack;

    private void Start()
    {
        IState[] states = new IState[2];
        states[0] = new IdleState(this, idlePoint, Speed);
        states[1] = new AggressiveState(this, Speed, timeBetweenAtack);

        StatesEnum[] statesEnums = new StatesEnum[2];
        statesEnums[0] = StatesEnum.Idle;
        statesEnums[1] = StatesEnum.Aggressive;

        base.Init(statesEnums, states);

        base.ChangeState(StatesEnum.Aggressive);
    }

    public void ChangeState()
    {
        stateMachine.NextState();
    }
}
