﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondNPC : NPC
{
    [SerializeField]
    private IdleDestinationPoint idlePoint;

    private void Start()
    {
        IState[] states = new IState[1];
        states[0] = new IdleState(this, idlePoint, Speed);

        StatesEnum[] statesEnums = new StatesEnum[1];
        statesEnums[0] = StatesEnum.Idle;

        base.Init(statesEnums, states);

        base.ChangeState(StatesEnum.Idle);
    }
}
