﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Vector3 direction = Vector3.zero;

    private float force;

    private int bulletDamage = 5;

    public void Init(Vector3 direction, float force)
    {
        this.direction = direction;
        this.force = force;
    }

    private void Update()
    {
        if (direction == Vector3.zero)
            return;

        transform.position += direction * (Time.deltaTime * force);
    }

    private void OnTriggerEnter(Collider other)
    {
        var npc = other.gameObject.GetComponent<NPC>();
        
        if(npc != null)
            npc.GetDamage(bulletDamage);
    }
}
