﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class NPC : MonoBehaviour
{
    protected StateMachine stateMachine;

    private Rigidbody rigidbody;

    public Rigidbody Rigidbody { get { return rigidbody; } }

    [SerializeField]
    private float hp;
    public float Hp { get { return hp; } }

    [SerializeField]
    private float speed;
    public float Speed { get { return speed; } }

    public virtual void Init(StatesEnum[] stateEnums, IState[] states)
    {
        stateMachine = GetComponent<StateMachine>();

        for (int i = 0; i < stateEnums.Length; i++)
        {
            stateMachine.AddState(stateEnums[i], states[i]);
        }

        rigidbody = GetComponent<Rigidbody>();
    }

    public virtual void ChangeState(StatesEnum stateEnum)
    {
        stateMachine.SetState(stateEnum);
    }

    public virtual void GetDamage(int damage)
    {
        hp -= damage;
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }
}