﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AggressiveState : IState
{
    private SecondNPC targetNpc;

    private NPC npc;

    private float speed;
    private float timeBetweenAtack;
    private float timer;

    public AggressiveState(NPC npc, float speed, float timeBetweenAtack)
    {
        this.npc = npc;
        this.speed = speed;
        this.timeBetweenAtack = timeBetweenAtack;
    }

    public void StateExecution()
    {
        timer += Time.deltaTime;

        if (targetNpc == null)
            targetNpc = Object.FindObjectOfType<SecondNPC>();

        if(targetNpc != null)
        {
            Rotate();

            if ((targetNpc.transform.position - npc.transform.position).sqrMagnitude <= 25 && timeBetweenAtack < timer)
            {
                timer = 0;
                Atack();
            }
            else if((targetNpc.transform.position - npc.transform.position).sqrMagnitude >= 25)
                Move();
        }

    }

    public void StateTransition()
    {
        if(targetNpc.Hp <= 0)
        {
            npc.ChangeState(StatesEnum.Idle);
        }
    }

    private void Move()
    {
        Vector3 newPos = Vector3.MoveTowards(npc.transform.position, targetNpc.transform.position, speed * Time.deltaTime);

        npc.Rigidbody.MovePosition(newPos);
    }

    private void Rotate()
    {
        Vector3 direction = (targetNpc.transform.position - npc.transform.position).normalized;

        if (direction == Vector3.zero)
            return;

        Quaternion lookRotation = Quaternion.LookRotation(direction * Time.deltaTime);
        npc.Rigidbody.MoveRotation(lookRotation);
    }

    private void Atack()
    {
        var bullet = CreateBullet();
        bullet.Init(npc.transform.forward, 50f);
    }

    private Bullet CreateBullet()
    {
        var bulletGO = GameObject.CreatePrimitive(PrimitiveType.Sphere);

        bulletGO.transform.position = npc.transform.position;
        bulletGO.AddComponent<Rigidbody>();
        bulletGO.GetComponent<SphereCollider>().isTrigger = true;

        var bullet = bulletGO.AddComponent<Bullet>();
        return bullet;
    }
}
