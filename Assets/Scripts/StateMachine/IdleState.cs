﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IState
{
    private NPC npc;

    [SerializeField]
    private IdleDestinationPoint idlePoint;

    private float speed;
    private float timerForRelax = 0;

    private int currentPointIndex = 0;

    private bool isCanMove = true;

    public IdleState(NPC npc, IdleDestinationPoint idlePoint, float speed)
    {
        this.npc = npc;
        this.idlePoint = idlePoint;
        this.speed = speed;
    } 

    public void StateExecution()
    {
        timerForRelax += Time.deltaTime;

        if (Move(idlePoint.points[currentPointIndex]))
        {
            if (currentPointIndex + 1 < idlePoint.points.Length)
            {
                currentPointIndex++;
            }
            else
                currentPointIndex = 0;
        }

        if(timerForRelax >= idlePoint.timerForRelax)
        {
            npc.StartCoroutine(RelaxCoroutine());
            timerForRelax = 0;
        }
    }

    public void StateTransition()
    {
        if (npc.Hp <= 0)
            npc.Die();
    }

    private bool Move(Vector3 newPoint)
    {
        if (!isCanMove)
            return false;

        Vector3 newPos = Vector3.MoveTowards(npc.transform.position, newPoint, speed * Time.deltaTime);

        npc.Rigidbody.MovePosition(newPos);

        if ((npc.transform.position - newPoint).sqrMagnitude <= 0.04)
            return true;
        else
            return false;
    }

    private IEnumerator RelaxCoroutine()
    {
        isCanMove = false;

        float timer = Random.Range(idlePoint.relaxTime.x, idlePoint.relaxTime.y);

        yield return new WaitForSeconds(timer);
        isCanMove = true;
    }
}

[System.Serializable]
public class IdleDestinationPoint
{
    public Vector3[] points;
    [Header("From - to")]
    public Vector2 relaxTime;
    public float timerForRelax;
}
