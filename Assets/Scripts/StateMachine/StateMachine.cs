﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    private IState currentState;
    public IState CurrentState { get { return currentState; } }

    private StatesEnum currentStateKey;

    private Dictionary<StatesEnum, IState> statesMap = new Dictionary<StatesEnum, IState>();

    public void AddState(StatesEnum stateEnum, IState state)
    {
        statesMap.Add(stateEnum, state);
    }

    public void SetState(StatesEnum stateEnum)
    {
        if (statesMap.ContainsKey(stateEnum))
        {
            currentStateKey = stateEnum;
            currentState = statesMap[stateEnum];
        }
    }

    public void NextState()
    {
        int stateKey = (int)currentStateKey;

        if (stateKey + 1 < statesMap.Keys.Count)
            stateKey++;
        else
            stateKey = 0;

        StatesEnum nextStateKey = (StatesEnum)Enum.ToObject(typeof(StatesEnum), stateKey);

        SetState(nextStateKey);
    }

    private void Update()
    {
        currentState.StateExecution();
        currentState.StateTransition();
    }
}

public enum StatesEnum
{
    Idle = 0,
    Aggressive = 1,
}
